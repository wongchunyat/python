import requests
import json
from dotenv import load_dotenv
import os

# Get Configuration Settings
load_dotenv()
cog_key = os.getenv('COG_SERVICE_KEY')
cog_region = os.getenv('COG_SERVICE_REGION')
translator_endpoint = 'https://api.cognitive.microsofttranslator.com'

# Use the Translator translate function
path = '/translate'
url = translator_endpoint + path

# Build the request

headers = {
    'Ocp-Apim-Subscription-Key': cog_key,
    'Ocp-Apim-Subscription-Region': cog_region,
    'Content-type': 'application/json'
}


def translate(text: str = 'Hello, welcome to una', fromLang: str = 'en', toLang = ['ja', 'fr']) -> str:  
    params = {
        'api-version': '3.0',
        'from': fromLang,
        'to': toLang,
    }
    body = [{
        'text': text
    }]
    
    # Send the request and get response
    request = requests.post(url, params=params, headers=headers, json=body)
    response = request.json()
    
    translation = response[0]["translations"]
    return translation
