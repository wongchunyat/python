from html import entities
from dotenv import load_dotenv
import os
import json
import chardet
from azure.core.credentials import AzureKeyCredential
from azure.ai.textanalytics import TextAnalyticsClient

load_dotenv()

endpoint = os.environ["COG_SERVICE_ENDPOINT"]
key = os.environ["COG_SERVICE_KEY"]
region = os.environ["COG_SERVICE_REGION"]

credentials = AzureKeyCredential(key)

client = TextAnalyticsClient(endpoint, credentials)

files = [f"reviews/{filename}" for filename in sorted(os.listdir("reviews/"))]

langs = list(map(lambda a: a["primary_language"]["iso6391_name"], client.detect_language(
    [open(file).read() for file in files])))

sentiments = list(map(lambda a: {"sentiment": a["sentiment"], "confidence": dict(a["confidence_scores"])}, client.analyze_sentiment(
    [open(file).read() for file in files])))

entities = list(map(lambda a: list(map(lambda b: dict(b), a.entities)), client.recognize_entities(
    [open(file).read() for file in files])))

linked_entities = list(map(lambda a: list(map(lambda b: {"name": b.name, "url": b.url}, a.entities)), client.recognize_linked_entities(
    [open(file).read() for file in files])))

print(json.dumps([{"file": files[i], "lang": langs[i], "sentiment": sentiments[i], "entities": entities[i], "linked_entities": linked_entities}
                  for i in range(len(files))], indent=4, sort_keys=False))
