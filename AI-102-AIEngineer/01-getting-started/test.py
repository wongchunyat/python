from dotenv import load_dotenv
import os
from azure.ai.textanalytics import TextAnalyticsClient
from azure.core.credentials import AzureKeyCredential

load_dotenv()
cog_endpoint = os.environ['COG_SERVICE_ENDPOINT']
cog_key = os.environ['COG_SERVICE_KEY']

credential = AzureKeyCredential(cog_key)
client = TextAnalyticsClient(endpoint=cog_endpoint, credential=credential)


def test(language: str, test_amount: int = 50):
    words = list(map(lambda x: x.lower().replace("\n", ""), open(f"../../words/src/resources/{language.lower()}.txt")))[:test_amount]
    detectedLanguage = list(map(lambda x: x.primary_language.name.split("_")[0],
                                client.detect_language(documents=words)))
    if detectedLanguage != [language for _ in detectedLanguage]:
        for i in range(0, len(detectedLanguage)):
            if (detectedLanguage[i] != language):
                print(
                    f"Word: {words[i]}, Detected: {detectedLanguage[i]}")
    else:
        print("Success")
    accuracy = detectedLanguage.count(language) / test_amount
    print(f"Accuracy: {accuracy}")

test(input("Language: "), 1000)
