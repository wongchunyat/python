from playsound import playsound
from azure.core.credentials import AzureKeyCredential
import azure.cognitiveservices.speech as speechsdk
from dotenv import load_dotenv
import os

load_dotenv()

key = os.environ["COG_SERVICE_KEY"]
endpoint = os.environ["COG_SERVICE_ENDPOINT"]
region = os.environ["COG_SERVICE_REGION"]

# credentials = AzureKeyCredential(key=key)


audioFile = "rick.wav"
# playsound(audioFile)

speech_config = speechsdk.SpeechConfig(subscription=key, region=region)
print('Ready to use speech service in:', speech_config.region)

audio_config = speechsdk.AudioConfig(filename=audioFile)
speech_recognizer = speechsdk.SpeechRecognizer(speech_config, audio_config)

speech_synthesizer = speechsdk.SpeechSynthesizer(speech_config, speechsdk.audio.AudioOutputConfig(use_default_speaker=True))
# print("Speak into your microphone.")

speech_recognition_result = speech_recognizer.recognize_once_async().get()

if speech_recognition_result.reason == speechsdk.ResultReason.RecognizedSpeech:
    print(f"Recognized: {speech_recognition_result.text}")
    speech_synthesizer.speak_text_async(speech_recognition_result.text).get()

elif speech_recognition_result.reason == speechsdk.ResultReason.NoMatch:
    print(
        f"No speech could be recognized: {speech_recognition_result.no_match_details}")

elif speech_recognition_result.reason == speechsdk.ResultReason.Canceled:
    cancellation_details = speech_recognition_result.cancellation_details
    print(f"Speech Recognition canceled: {cancellation_details.reason}")
    if cancellation_details.reason == speechsdk.CancellationReason.Error:
        print("Error details: {}".format(
            cancellation_details.error_details))
        print("Did you set the speech resource key and region values?")
