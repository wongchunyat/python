# Speech sdk
import azure.cognitiveservices.speech as speech_sdk
# library for load audio file
from playsound import playsound
# other
from dotenv import load_dotenv
import os
import sys
sys.path.append(os.path.join(os.path.dirname(__file__), '..'))
import translate

# Get Configuration Settings
load_dotenv()
cog_key = os.getenv('COG_SERVICE_KEY')
cog_region = os.getenv('COG_SERVICE_REGION')

# Configure speech service
speech_config = speech_sdk.SpeechConfig(cog_key, cog_region)
print('Ready to use speech service in:', speech_config.region)

# Configure output audio format (Optional) (for file output)
speech_config.set_speech_synthesis_output_format(
    speech_sdk.SpeechSynthesisOutputFormat.Riff24Khz16BitMonoPcm)

# For audio file output (Optional to add)
file_name = "output.wav"
audio_config = speech_sdk.audio.AudioOutputConfig(filename=file_name)

# Configure speech synthesizer
speech_config.speech_synthesis_voice_name = "en-HK-YanNeural"
speech_synthesizer = speech_sdk.SpeechSynthesizer(speech_config)

# Configure speech synthesis (for file output)
# speech_config.speech_synthesis_voice_name = "en-GB-RyanNeural"
# speech_synthesizer = speech_sdk.SpeechSynthesizer(speech_config, audio_config)

text = 'Hello, welcome to una'

# Synthesize spoken output
speak = speech_synthesizer.speak_text_async(text).get()
if speak.reason != speech_sdk.ResultReason.SynthesizingAudioCompleted:
    print(speak.reason)


responseSsml = f'''
<speak version="1.0" xmlns="http://www.w3.org/2001/10/synthesis"
       xmlns:mstts="https://www.w3.org/2001/mstts" xml:lang="en-US">
    <voice name="en-US-JennyNeural">
        <break strength="strong" />
        microsoft is evil, linux better
        <break strength="weak" />
        <mstts:express-as style="fearful">
            {translate.translate(text).jp}
        </mstts:express-as>
    </voice>
</speak>
'''

speak = speech_synthesizer.speak_ssml_async(responseSsml).get()
if speak.reason != speech_sdk.ResultReason.SynthesizingAudioCompleted:
    print(speak.reason)
