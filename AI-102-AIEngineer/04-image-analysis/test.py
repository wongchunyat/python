from dotenv import load_dotenv
import os
from PIL import Image, ImageDraw
import sys
from matplotlib import pyplot as plt
from azure.cognitiveservices.vision.computervision import ComputerVisionClient
from azure.cognitiveservices.vision.computervision.models import VisualFeatureTypes
from msrest.authentication import CognitiveServicesCredentials
from termcolor import colored
from PIL import Image, ImageDraw

load_dotenv()
cog_endpoint = os.getenv('COG_SERVICE_ENDPOINT')
cog_key = os.getenv('COG_SERVICE_KEY')

credential = CognitiveServicesCredentials(cog_key)
cv_client = ComputerVisionClient(cog_endpoint, credential)

image_file = 'images/street.jpg'
image_data = open(image_file, mode="rb")

analysis = cv_client.analyze_image_in_stream(
    image_data, VisualFeatureTypes)

print(colored("Categories:", "blue"))
print("\n".join(map(lambda a: f'category: {a.name}, confidence: {colored(round(a.score, 2), "green" if a.score > 0.5  else "red")}', analysis.categories)))

print(colored("Tags:", "blue"))
print("\n".join(analysis.description.tags))

print(colored("Captions:", "blue"))
print("\n".join(map(lambda a: f'caption: {a.text}, confidence: {colored(round(a.confidence, 2), "green" if a.confidence > 0.5  else "red")}', analysis.description.captions)))

print(colored(f"adult score: {analysis.adult.adult_score}", "red"))

im = Image.open(image_file)
draw = ImageDraw.Draw(im)
for object in analysis.objects:
    print(f'Object: {object.object_property}')
    print(f'Rectange: {object.rectangle}')
    x = object.rectangle.x
    y = object.rectangle.y
    w = object.rectangle.w
    h = object.rectangle.h
    draw.rectangle((x, y, x+w, y+h), outline=(255,0,0), width=2)
# im.show()
im.save("test.jpg")

# Generate a thumbnail
with open(image_file, mode="rb") as image_data:
# Get thumbnail data
    thumbnail_stream = cv_client.generate_thumbnail_in_stream(100, 100, image_data, True)

# Save thumbnail image
thumbnail_file_name = 'thumbnail.png'
with open(thumbnail_file_name, "wb") as thumbnail_file:
    for chunk in thumbnail_stream:
        thumbnail_file.write(chunk)

print('Thumbnail saved in.', thumbnail_file_name)
