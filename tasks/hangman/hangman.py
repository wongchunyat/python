#!/usr/bin/env python
# coding: utf-8

# In[ ]:


import random
word_file = "./words.txt"
WORDS = list(filter(lambda i: i.isalpha() and len(i) < 15 and len(i) > 5, open(word_file).read().splitlines()))


# In[ ]:


class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKCYAN = '\033[96m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'


# In[ ]:


pics = ['''
	  +---+
	  |   |
	      |
	      |
	      |
	      |
	=========''', '''
	  +---+
	  |   |
	  O   |
	      |
	      |
	      |
	=========''', '''
	  +---+
	  |   |
	  O   |
	  |   |
	      |
	      |
	=========''', '''
	  +---+
	  |   |
	  O   |
	 /|   |
	      |
	      |
	=========''', '''
	  +---+
	  |   |
	  O   |
	 /|\  |
	      |
	      |
	=========''', '''
	  +---+
	  |   |
	  O   |
	 /|\  |
	 /    |
	      |
	=========''', '''
	  +---+
	  |   |
	  O   |
	 /|\  |
	 / \  |
	      |
	=========''']


# In[ ]:


def newGame():
    wrong = []
    correct = []
    word = list(WORDS[random.randint(0, len(WORDS))])
    guess = ["_" for _ in word]
    chances = 6
    print(f'Word: {" ".join(guess)}')
    while (word != guess):
        n = input("Guess a letter: ").lower()
        print("")
        if (len(n) != 1 or not n.isalpha()):
            print("Invalid\n")
            continue
        if (n in wrong or n in correct):
            print("You have guessed this already!\n")
            continue
        if n in word:
            correct.append(n)
            guess = [i if i in correct else "_" for i in word]
            print(f"{bcolors.OKGREEN}{n} is correct!{bcolors.ENDC}\n")
            if (word == guess):
                print(
                    f'You got the correct answer {"".join(word)} in {len(wrong)} times!')
        elif n:
            wrong.append(n)
            print(f"{bcolors.FAIL}{n} is wrong!{bcolors.ENDC}\n")
        print(f'{bcolors.OKBLUE}Word: {" ".join(guess)}{bcolors.ENDC}')
        print(f"{bcolors.WARNING}{pics[len(wrong)]}{bcolors.ENDC}")
        print("")
        print(f"{chances - len(wrong)} chance(s) left\n")
        print(f'{bcolors.OKGREEN}Correct: {", ".join(correct)}{bcolors.ENDC}')
        print(f"{bcolors.FAIL}Wrong: {', '.join(wrong)}{bcolors.ENDC}\n")
        if (len(wrong) >= chances):
            print(f'Game over. The word was {"".join(word)}.\n')
            break


# In[ ]:


newGame()
while input("Play new game? (y/n) ") == "y":
    print("")
    newGame()

